let sorting = (array) => {
    return array.sort();
}

let compare = (a, b) => {
    if (Number(a['PM2.5']) > Number(b['PM2.5'])) {
        return 1;
    }
    if (Number(a['PM2.5']) < Number(b['PM2.5'])) {
        return -1; 
    }
    return 0;

}

let average = (nums) => {
    let sum = nums.reduce((previous, current) => current += previous);
	   return Math.floor(100*sum / nums.length) / 100;
}


module.exports = {
    sorting,
    compare,
    average
}